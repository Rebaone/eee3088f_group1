# EEE3088F_Group1

This is a Git repository for the Electrical Engineering design (EEE3088F) project. The team has been tasked to design an Enviro sensing board(HAT) which will be attached to an STM32 discovery board.

## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/Rebaone/eee3088f_group1.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/Rebaone/eee3088f_group1/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***
# Light and Temperature Sensor HAT
## Description

The following project consists of a design for an enviro-sensing HAT which has a temperature sensor and a light sensor. The hat can be used for measuring temperatures in a room/greenhouse farms and the changes in light detected in a room or any place.
## Hardware needed

STM32 microcontroller and a temperature sensor IC(LM75A) and a Light Dependent Resistor (LDR)

A USB cable.

A battery or micro-USB cable for power supply

EEPROM Programming software

## Software

KiCAD was used to produce the circuit schematics.

Putty was used to interface the laptop’ serial port with the discovery board.

STM32IDE cube was used to program the stm32 discovery board.

## Connecting hardware

Connect the LM75A to the STM32 microcontroller.

Connect LDR with a resistor to form a voltage divider. Connect the output of the voltage divider to the ADC input of the microcontroller.

Connect the battery or USB cable to power the whole hat.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## Authors and acknowledgment
Koopalame Rebaone

Thato Khoabane

Wiseman Ngubane


## License
[GNU GPL(General Public Licence)](https://choosealicense.com/licenses/mit/)


